# AutomateGo
# author : M Rahman Darmawan
# email  : rahman.drm21@gmail.com


#===========How To Run===========#
- First clone project on https://gitlab.com/mr_darmawan21/seleniumtest.git
- Open project use eclipse
- You just run the build.xml
- Right click and run as ant build
- For start running automate 
- Run as junit on class Runner.java